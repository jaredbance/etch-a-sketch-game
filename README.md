# Etch-a-Sketch Game Android App

This game requires two players:

* Player 1 is given a random prompt
* Player must draw the prompt within the given time limit
* Afterwords, Player 2 chooses from a selection of options what they think is depicted in the drawing

## Development Process

See "Documentation_of_development_process.pdf" for more details

## Video Demo

https://youtu.be/P1g87kDkTwU

## Authors

* **Jared Bance**
* **Jeff Datahan**
* **Victor Lieu**