package com.example.jared.switchcontrollstwo;

/** Handles all game mechanics except for drawing
 * @author Jared Bance
 * @author Victor Lieu
 * @author Jeff Datahan
 */

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import io.github.controlwear.virtual.joystick.android.JoystickView;
import android.graphics.Bitmap;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.Toast;

import com.veer.hiddenshot.HiddenShot;
import java.util.Random;


public class MainActivity extends AppCompatActivity {


    //////VARIABLES NEEDED FOR SHAKE SENSOR////////////////////////////////
    private SensorManager sensorM;                                       //
    private float acelVal;      // Current Acceleration Value and gravity//
    private float acelLast;     // Last Acceleration Value and gravity   //
    private float shake;        // Acceleration Value differ from gravity//////////////////////////////////////////
    private int minShake = 5;   // Amount users needs to shake device in order for device to register a response //
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////


    private String[] prompts = {"Eggplant","House","T-Shirt","Pants","Shoe","Purse","Pencil","Dinosaur"}; // Prompts that can be given to player 1 & 2
    private int numberOfPrompts = prompts.length - 1;
    private String currentPrompt;
    Random r = new Random();    // random number used to randomly select a prompt

    private TextView Prompt;
    private TextView pleaseDrawLabel;
    private TextView timerTextLabel;
    private TextView textBackground;
    private Button guess1;
    private Button guess2;
    private Button guess3;
    private Button results;
    private TextView selectGuessLabel;
    //private Button[] guesses = {guess1,guess2,guess3};

    private DrawingLine drawingView;
    private View main;
    private ImageView imageview;

    private TextView timerText;

    private static CountDownTimer timer;
    private boolean doesTimerExist = false;
    private boolean doesJoysticksExist = false;
    private Button finish;

    private Button startButton;
    private Button playerOneReady;
    private Button playerTwoReady;
    private TextView bugfix;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.startscreen);

        // if user clicks start button then user is displayed new button that asks if they are ready to start
        startButton = (Button) findViewById(R.id.StartButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playerOneReady();
            }
        });
    }

    // function for view displaying button/message asking player 1 to get ready
    // this function is only used no previous rounds have been played
    // If previous rounds have been player, use playerOneReadyAlt() instead

    /**
     * Function for view displaying button/message asking player 1 to get ready.
     * This function is ONLY used if no previous rounds have been played.
     * If previous rounds have been played, use playerOneReadyAlt() instead.
     */
    public void playerOneReady(){
        setContentView(R.layout.p1getready);
        playerOneReady = (Button)findViewById(R.id.PlayerOneReady);
        playerOneReady.setText("ARE YOU READY PLAYER 1? \n \n CLICK HERE TO START GAME");
        playerOneReady.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(R.layout.activity_main);
                final JoystickView joystickLeft = (JoystickView) findViewById(R.id.joystickView_left);
                final JoystickView joystickReft = (JoystickView) findViewById(R.id.joystickView_right);

                start();
            }
        });
    }

    /**
     * Same as playerOneRead() except used when previous rounds have been player
     * If no previous rounds have been played yet, use playerOneReady()
     * @param joystickLeft left joystick object
     * @param joystickRight right joystick object
     */
    public void playerOneReadyAlt(final JoystickView joystickLeft, final JoystickView joystickRight){
        setContentView(R.layout.p1getready);
        playerOneReady = (Button)findViewById(R.id.PlayerOneReady);
        playerOneReady.setText("ARE YOU READY PLAYER 1? \n \n CLICK HERE TO START GAME");
        playerOneReady.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(R.layout.activity_main);
                timer.start();
                joystickLeft.resetButtonPosition();
                joystickRight.resetButtonPosition();
                joystickLeft.setEnabled(true);
                joystickRight.setEnabled(true);
                joystickLeft.setVisibility(View.VISIBLE);
                joystickRight.setVisibility(View.VISIBLE);
                bugfix.setVisibility(View.INVISIBLE);
                guess1.setVisibility(View.INVISIBLE);
                guess2.setVisibility(View.INVISIBLE);
                guess3.setVisibility(View.INVISIBLE);
                selectGuessLabel.setVisibility(View.INVISIBLE);
                timerText.setVisibility(View.VISIBLE);
                finish.setVisibility(View.VISIBLE);

                timerTextLabel.setVisibility(View.INVISIBLE);
                pleaseDrawLabel.setVisibility(View.INVISIBLE);
                textBackground.setVisibility(View.VISIBLE);


                timer.start();
                setPrompt();
                randomSetGuesses();
                Prompt.setVisibility(View.VISIBLE);

                results.setVisibility(View.INVISIBLE);
                drawingView.restart();
                minShake = 5;
                start();
            }
        });
    }


    /**
     * This function starts the game from the beginning
     */
    public void start(){
        // Show and activate the save drawing button
        Button btn = (Button) findViewById(R.id.saveBtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawingView.setDrawingCacheEnabled(true);
                drawingView.buildDrawingCache();
                Bitmap shot = drawingView.getDrawingCache();
                //Bitmap shot = HiddenShot.getInstance().buildShot(MainActivity.this);
                HiddenShot.getInstance().saveShot(MainActivity.this, shot, "view");
                drawingView.setDrawingCacheEnabled(false);
                Toast toast= Toast.makeText(MainActivity.this, "DRAWING SAVED!", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER|Gravity.CENTER, 0, 0);
                toast.show();
            }
        });

        // Create instances of various gameplay elements for later
        Prompt = (TextView) findViewById(R.id.Prompt);
        Prompt.setVisibility(View.VISIBLE);
        currentPrompt = prompts[r.nextInt(numberOfPrompts)];
        Prompt.setText(currentPrompt);
        results = (Button) findViewById(R.id.Results);
        timerTextLabel = (TextView)findViewById(R.id.TimeLabel);
        pleaseDrawLabel = (TextView)findViewById(R.id.PleaseDraw);
        textBackground = (TextView)findViewById(R.id.TextBackground);
        selectGuessLabel = (TextView)findViewById(R.id.SelectGuessLabel);
        bugfix = (TextView)findViewById(R.id.BugFix);

        drawingView = (DrawingLine) findViewById(R.id.DrawXml);

        drawingView.initPen();




        ///////////////////////////
        // LEFT JOYSTICK LISTENER
        //////////////////////////
        final JoystickView joystickLeft = (JoystickView) findViewById(R.id.joystickView_left);
        joystickLeft.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {
                // Change direction of path according to direction control stick is moved
                System.out.println(strength);
                if (angle == 180) {
                    strength = strength * -1;
                }
                drawingView.getCoords(strength / 10, 0);
                drawingView.invalidate(); // <need to call this in order for DrawingLine.onDraw() to be called
            }
        });
        ////////////////////////////
        // RIGHT JOYSTICK LISTENER
        ///////////////////////////
        final JoystickView joystickRight = (JoystickView) findViewById(R.id.joystickView_right);
        joystickRight.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {
                // Change direction of path according to direction control stick is moved
                if (angle == 90) {
                    strength = strength * -1;
                }
                drawingView.getCoords(0, strength / 10);
                drawingView.invalidate(); // <need to call this in order for DrawingLine.onDraw() to be called
            }
        });


        // activate "shake to erase" game play mechanic
        sensorM = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorM.registerListener(sensorListener, sensorM.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);

        acelVal = SensorManager.GRAVITY_EARTH;
        acelLast = SensorManager.GRAVITY_EARTH;
        shake = 0.00f;


        // display and activate the timer
        timerText = (TextView) findViewById(R.id.timeText);

        // dont create a new timer if a previous timer has been created
        if (doesTimerExist == false) {

            timer = new CountDownTimer(30000, 1000) {
                public void onTick(long millisUntilFinished) {
                    timerText.setText("" + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    //timerText.setText("done!");

                    joystickLeft.resetButtonPosition();
                    joystickRight.resetButtonPosition();
                    joystickLeft.setEnabled(false);
                    joystickRight.setEnabled(false);
                    joystickLeft.setVisibility(View.INVISIBLE);
                    joystickRight.setVisibility(View.INVISIBLE);
                    bugfix.setVisibility(View.VISIBLE);


                    timerTextLabel.setVisibility(View.INVISIBLE);
                    pleaseDrawLabel.setVisibility(View.INVISIBLE);
                    textBackground.setVisibility(View.INVISIBLE);
                    finish.setVisibility(View.INVISIBLE);

                    //playerTwoReady();

                    playerTwoReady = (Button) findViewById(R.id.ReadyPlayer2);
                    playerTwoReady.setText("PLEASE GIVE THE PHONE TO PLAYER 2 \n \n CLICK TO CONTINUE");
                    playerTwoReady.setVisibility(View.VISIBLE);

                    playerTwoReady.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            playerTwoReady.setVisibility(View.INVISIBLE);


                        }
                    });

                    guess1.setVisibility(View.VISIBLE);
                    guess2.setVisibility(View.VISIBLE);
                    guess3.setVisibility(View.VISIBLE);
                    selectGuessLabel.setVisibility(View.VISIBLE);
                    Prompt.setVisibility(View.INVISIBLE);
                    timerText.setVisibility(View.INVISIBLE);
                    minShake = 1000000;
                }
            };

            doesTimerExist = true;
        }
        timer.start();

        // Implementation of "Finished" button that ends round early if tapped
        finish = (Button)findViewById(R.id.Finished);
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                finished(joystickLeft,joystickRight);
            }
        });


        // 3 buttons that are displayed to player 2. One of them is labeled with the correct guess.
        guess1 = (Button) findViewById(R.id.Guess1);
        guess2 = (Button) findViewById(R.id.Guess2);
        guess3 = (Button) findViewById(R.id.Guess3);
        randomSetGuesses();
        setOnGuessListerner(guess1,joystickLeft,joystickRight);
        setOnGuessListerner(guess2,joystickLeft,joystickRight);
        setOnGuessListerner(guess3,joystickLeft,joystickRight);
    }

    /**
     *  This function is called when player 1 is done drawing the prompt
     *  This can happen by either the timer running out or player 1 taps the "Finished!" button
     * @param joystickLeft left joystick object
     * @param joystickRight right joystick object
     */
    private void finished(JoystickView joystickLeft, JoystickView joystickRight){
        joystickLeft.resetButtonPosition();
        joystickRight.resetButtonPosition();
        joystickLeft.setEnabled(false);
        joystickRight.setEnabled(false);
        joystickLeft.setVisibility(View.INVISIBLE);
        joystickRight.setVisibility(View.INVISIBLE);
        bugfix.setVisibility(View.VISIBLE);

        timerTextLabel.setVisibility(View.INVISIBLE);
        pleaseDrawLabel.setVisibility(View.INVISIBLE);
        textBackground.setVisibility(View.INVISIBLE);
        finish.setVisibility(View.INVISIBLE);

        playerTwoReady();

        guess1.setVisibility(View.VISIBLE);
        guess2.setVisibility(View.VISIBLE);
        guess3.setVisibility(View.VISIBLE);
        selectGuessLabel.setVisibility(View.VISIBLE);
        Prompt.setVisibility(View.INVISIBLE);
        timerText.setVisibility(View.INVISIBLE);
        minShake = 1000000; // set minimum shake value very high so player 2 can't erase drawing
    }

    /**
     * This function displays button with label asking player 2 to get ready
     */
    private void playerTwoReady(){

        playerTwoReady = (Button)findViewById(R.id.ReadyPlayer2);
        playerTwoReady.setText("PLEASE GIVE THE PHONE TO PLAYER 2 \n \n CLICK TO CONTINUE");
        playerTwoReady.setVisibility(View.VISIBLE);

        playerTwoReady.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playerTwoReady.setVisibility(View.INVISIBLE);
               return;

            }
        });
    }

    /**
     * Listener for shake sensor.
     * Shake sensor is used for "shake to erase" game mechanic.
     */
    private final SensorEventListener sensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {

            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            acelLast = acelVal;
            acelVal = (float) Math.sqrt((double) (x*x + y*y + z*z));
            float delta = acelVal - acelLast;
            shake = shake * 0.9f + delta;

            if (shake > minShake) {
                Toast toast= Toast.makeText(MainActivity.this, "DRAWING CLEARED!", Toast.LENGTH_SHORT/10);
                toast.setGravity(Gravity.CENTER|Gravity.CENTER, 0, 0);
                //toast.show();

                //Toast.makeText(MainActivity.this, "DRAWING CLEARED!", Toast.LENGTH_SHORT).show();
                drawingView.clear();
                drawingView.invalidate();
            }

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };

    /**
     * Takes button parameter and checks if it's label contains the correct guess or not.
     * @param guess button containing guess that was tapped by player 2
     * @param joystickLeft left joystick object
     * @param joystickRight right joystick object
     */
    private void setOnGuessListerner(final Button guess, final JoystickView joystickLeft, final JoystickView joystickRight){
        guess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (guess.getText() == currentPrompt){
                   guessedCorrect(true, joystickLeft, joystickRight);
                }
                else{
                    guessedCorrect(false, joystickLeft, joystickRight);
                }
            }
        });

    }

    /**
     * Shows correct display for when player 2 guesses correct or incorrect
     * @param guess true if guess was correct, false if guess was incorrect
     * @param joystickLeft  left joystick object
     * @param joystickRight right joystick object
     */
    private void guessedCorrect(final boolean guess, final JoystickView joystickLeft, final JoystickView joystickRight){
        if (guess == true){
            results.setText("You Guessed Correct!" + "\n \n" +"Click Here to Play Again");
        }
        else{
            results.setText("Your Guess Was Incorrect!" + "\n \n" +"Click Here to Play Again");
        }

        results.setVisibility(View.VISIBLE);

        results.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playerOneReadyAlt(joystickLeft,joystickRight);
            }
        });
    }


    /**
     * Set prompt that displays message containing a 'thing' for player 1 to draw.
     * Checks to make sure new prompt isn't the same as the old prompt.
     */
    private void setPrompt(){
        String oldPrompt = currentPrompt;
        currentPrompt = prompts[r.nextInt(numberOfPrompts)];
        while (currentPrompt == oldPrompt){
            currentPrompt = prompts[r.nextInt(numberOfPrompts)];
        }
        Prompt.setText(currentPrompt);
    }

    /**
     * Change labels on the three guess buttons
     */
    private void randomSetGuesses(){
        Button[] guesses = {guess1,guess2,guess3};
        int RandomButton = r.nextInt(guesses.length); // The correct guess button is randomly chosen
        String previousIncorrectGuess = null;

        for (int guess = 0; guess < guesses.length; guess++){
            if (guess == RandomButton){ // only want one button to have correct guess
                guesses[guess].setText(currentPrompt);
            }
            else {
                int randomIncorrectGuess = r.nextInt(prompts.length);
                // make sure random incorrect guesses chosen are NOT the same as the randomly chosen correct guess
                while (prompts[randomIncorrectGuess] == currentPrompt || prompts[randomIncorrectGuess] == previousIncorrectGuess){
                    randomIncorrectGuess = r.nextInt(prompts.length);
                }
                previousIncorrectGuess = prompts[randomIncorrectGuess];
                guesses[guess].setText(prompts[randomIncorrectGuess]);
            }
        }


    }
}
