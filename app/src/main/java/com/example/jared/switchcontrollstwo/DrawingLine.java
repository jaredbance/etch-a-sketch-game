package com.example.jared.switchcontrollstwo;

/** Handles drawing game mechanic
 * @author Jared Bance
 * @author Victor Lieu
 * @author Jeff Datahan
 */

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.Region;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import static android.graphics.Bitmap.Config.ARGB_8888;


public class DrawingLine extends View /*implements View.OnClickListener */{

    Path line = new Path();
    Paint pen = new Paint();

    // variables that keep track of x and y position of line
    int current_x;
    int current_y;


    /**
     * Constructor sets desired pen settings
     * @param context
     * @param set
     */
    public DrawingLine(Context context, android.util.AttributeSet set) {
        super(context, set);
        pen.setColor(Color.BLACK);
        pen.setStrokeWidth(8f);
        pen.setStyle(Paint.Style.STROKE);
        pen.setFlags(Paint.ANTI_ALIAS_FLAG);
    }

    /**
     * Initiate pen by moving it to center of screen
     */
    public void initPen() {
        //line.moveTo(750,750);
        int width = Resources.getSystem().getDisplayMetrics().widthPixels;
        int length = Resources.getSystem().getDisplayMetrics().heightPixels;
        line.moveTo(((width/2)-50), ((length/2)-530));

        // Make a dot so user knows where pen is
        line.rLineTo(5,0);

        //update current_x and current_y
        current_x = (width/2) + 5 - 50;
        current_y = (length/2)-530;

    }

    /**
     * Offset current pen location by x and y parameters
     * Draws line between old coordinates and new coordinates
     * @param x offset of new x position
     * @param y offest of new y position
     */
    public void getCoords(int x, int y) {
        // HARD CODED RIGHT BOUNDARY
        if ((current_x + x) > 970){
            return;
        }
        current_x = current_x + x;
        current_y = current_y + y;

        line.rLineTo(x,y);
    }

    /**
     * This function is only called by invalidate()
     * Draws border of canvas
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        //draw border of canvas
        pen.setStrokeWidth(25f);
        canvas.drawRect(canvas.getClipBounds(),pen);
        pen.setStrokeWidth(8f);

        canvas.drawPath(line, pen);
    }

    /**
     * Clears the canvas
     */
    public void clear(){
        line.reset();
        line.moveTo(current_x,current_y);
        line.rLineTo(5,0);
        current_x = current_x + 5; // draw small dot so user can see current pen location on screen
    }

    /**
     * Resets pen for when game is restarted
     */
    public void restart(){
        line.reset();
        initPen();
    }
}
